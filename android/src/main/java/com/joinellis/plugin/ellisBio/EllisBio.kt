package com.joinKirillCodes.plugin.KirillCodesBio

import android.content.pm.PackageManager
import android.os.Build
import android.os.Handler
import android.os.Looper
import androidx.annotation.RequiresApi
import androidx.biometric.BiometricManager
import androidx.biometric.BiometricManager.Authenticators.*
import androidx.biometric.BiometricPrompt
import androidx.core.content.ContextCompat
import com.getcapacitor.JSObject
import com.getcapacitor.Plugin
import com.getcapacitor.PluginCall
import com.getcapacitor.PluginMethod
import com.getcapacitor.annotation.CapacitorPlugin

@RequiresApi(Build.VERSION_CODES.Q)
@CapacitorPlugin(name = "KirillCodesBio")
class KirillCodesBioPlugin : Plugin() {

    private var hasFaceId: Boolean? = false
    private var hasTouchId: Boolean? = false

    private lateinit var biometricManager: BiometricManager
    private lateinit var biometricPrompt: BiometricPrompt
    private lateinit var promptInfo: BiometricPrompt.PromptInfo

    override fun load() {
        super.load()

        hasFaceId = context.packageManager.hasSystemFeature(PackageManager.FEATURE_FACE);
        hasTouchId = context.packageManager.hasSystemFeature(PackageManager.FEATURE_FINGERPRINT);
        biometricManager = BiometricManager.from(context);
        promptInfo = BiometricPrompt.PromptInfo.Builder()
            .setTitle("Biometric login")
            .setSubtitle("Log in to your KirillCodes Account")
            .setAllowedAuthenticators(BIOMETRIC_STRONG or DEVICE_CREDENTIAL)
            .build()
    }

    @RequiresApi(Build.VERSION_CODES.Q)
    @PluginMethod
    fun bioAvailable(call: PluginCall) {
        if (hasTouchId!! && !hasFaceId!!) {
            call.resolve(JSObject().put("available", "TOUCH_ID"))
        } else if (hasTouchId!! || (hasFaceId!! && !hasTouchId!!)) {
            call.resolve(JSObject().put("available", "FACE_ID"))
        } else if (hasTouchId!! || (hasFaceId!! && !hasTouchId!!)) {
            call.resolve(JSObject().put("available", "FACE_ID"))
        } else {
            call.resolve(JSObject().put("available", "PIN"))
        }
    }

    @PluginMethod
    fun verify(call: PluginCall) {
        biometricPrompt = BiometricPrompt(activity, ContextCompat.getMainExecutor(context),
            object : BiometricPrompt.AuthenticationCallback() {
                override fun onAuthenticationError(errorCode: Int, errString: CharSequence) {
                    super.onAuthenticationError(errorCode, errString)
                    call.resolve(JSObject().put("ok", false).put("message", errString))
                }

                override fun onAuthenticationSucceeded( result: BiometricPrompt.AuthenticationResult ) {
                    super.onAuthenticationSucceeded(result)
                    call.resolve(JSObject().put("ok", true).put("message", "Verified"))
                }

                override fun onAuthenticationFailed() {
                    super.onAuthenticationFailed()
                    call.resolve(JSObject().put("ok", false).put("message", "something went wrong"))
                }
            }
        )

        Handler(Looper.getMainLooper()).post {
            notifyListeners("ready", JSObject().put("ok", true))
            biometricPrompt.authenticate(promptInfo)
        }
    }
}
