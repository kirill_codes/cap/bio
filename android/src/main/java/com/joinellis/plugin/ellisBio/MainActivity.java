package com.joinKirillCodes.plugin.KirillCodesBio;
import android.os.Build;
import android.os.Bundle;

import com.getcapacitor.BridgeActivity;

public class MainActivity extends BridgeActivity {
    @Override
    public void onCreate ( Bundle savedInstanceState ) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
            registerPlugin(KirillCodesBioPlugin.class);
        }
        super.onCreate(savedInstanceState);
    }
}
