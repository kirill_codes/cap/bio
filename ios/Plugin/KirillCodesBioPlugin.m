//
//  KirillCodesBioPlugin.m
//  Plugin
//
//  Created by Kirill Chernik on 9/6/22.
//

#import <Foundation/Foundation.h>
#import <Capacitor/Capacitor.h>

CAP_PLUGIN(KirillCodesBioPlugin, "KirillCodesBio",
    CAP_PLUGIN_METHOD(bioAvailable, CAPPluginReturnPromise);
    CAP_PLUGIN_METHOD(verify, CAPPluginReturnPromise);
)
