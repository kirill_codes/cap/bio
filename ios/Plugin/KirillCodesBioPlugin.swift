//  KirillCodesRatePlugin.swift
//  Plugin

//  Created by Kirill

import Capacitor
import Foundation
import LocalAuthentication
import UIKit


@objc(KirillCodesBioPlugin)
public class KirillCodesBioPlugin: CAPPlugin {
    var error: NSError?
    var available: String?;


    override public func load() {
        let lacontext = LAContext()

        NotificationCenter.default.addObserver(
            self, selector: #selector(self.printMe(notification:)),
            name: UIApplication.willResignActiveNotification, object:nil
        )


        if lacontext.canEvaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, error: &error) {
            switch lacontext.biometryType {
              case .touchID: available = "TOUCH_ID"
              case .faceID: available = "FACE_ID"
              default: available = "PIN"
            }
          }
          else { available = "PIN" }
    }

    @objc func printMe(notification: NSNotification) {
        self.notifyListeners("ready", data: [ "ok":  true, ]);
    }

    @objc func bioAvailable (_ call: CAPPluginCall) {
        call.resolve([ "available": available ?? "PIN", ]);
    }


    @objc public func verify (_ call: CAPPluginCall) {
        let lacontext = LAContext()

        if ((available) != nil && available != "PIN") {
            lacontext.evaluatePolicy(
                .deviceOwnerAuthenticationWithBiometrics,
                localizedReason: "Log in to your Account"
            ) { success, error in
                if success {
                    call.resolve([ "ok": true, "message": "Verified" ])
                } else {
                    call.resolve([
                        "ok": false,
                        "message": error?.localizedDescription ?? "something went wrong"
                    ]);
                }
            }
        } else {
            call.resolve([
              "ok": false,
              "message": "something went wrong"
            ]);
        }
    }
}
