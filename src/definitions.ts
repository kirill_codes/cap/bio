
import type { ListenerCallback, PluginListenerHandle } from "@capacitor/core";

export interface KirillCodesBioPlugin {
  bioAvailable ( ): Promise<{
    available: string;
  }>;

  verify ( ): Promise<{
    ok: boolean; message?: string;
  }>;

  addListener(
    eventName: string,
    handler: ListenerCallback
  ): Promise<PluginListenerHandle> & PluginListenerHandle;
}
