
import { registerPlugin } from "@capacitor/core";

import type { KirillCodesBioPlugin } from "./definitions";

const KirillCodesBio = registerPlugin<KirillCodesBioPlugin>("KirillCodesBio", {
  async web ( ) {
    return import("./web").then(function getBioWeb ( module ) {
      return new module.KirillCodesBioWeb();
    });
  },
});

export { KirillCodesBio, KirillCodesBioPlugin };
