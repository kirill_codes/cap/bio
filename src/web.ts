
import type { KirillCodesBioPlugin } from "./definitions";
import { WebPlugin } from "@capacitor/core";
import type { ListenerCallback, PluginListenerHandle } from "@capacitor/core";

// eslint-disable-next-line max-len
export class KirillCodesBioWeb extends WebPlugin implements KirillCodesBioPlugin {

  public async bioAvailable ( ): Promise<{
    available: string;
  }> {
    return Promise.resolve({ available: "false", });
  }

  public async verify ( ): Promise<{
    ok: boolean; message?: string;
  }> {
    return Promise.resolve({
      ok: false, message: "Not impleented on Web",
    });
  }

  public async addListener (
    _eventName: string, _listenerFunc: ListenerCallback
    // @ts-expect-error meow
  ): Promise<PluginListenerHandle> & PluginListenerHandle {
    return <PluginListenerHandle><unknown>null;
  }

}
